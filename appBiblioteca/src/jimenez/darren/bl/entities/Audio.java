package jimenez.darren.bl.entities;

import java.util.Arrays;
import java.util.Date;

public class Audio extends Audio_Visual {
    public Audio(String formato, String duracion, String idioma) {
        super(formato, duracion, idioma);
    }

    public Audio(String signatura, Date fechaCompra, String restringido, String tema, String formato, String duracion, String idioma) {
        super(signatura, fechaCompra, restringido, tema, formato, duracion, idioma);
    }

    public Audio() {
    }

    @Override
    public String toString() {
        return "Audio{" +
                "formato=" + formato +
                ", duracion='" + duracion + '\'' +
                ", idioma='" + idioma + '\'' +
                ", signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
