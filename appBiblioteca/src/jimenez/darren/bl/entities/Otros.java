package jimenez.darren.bl.entities;

public class Otros extends Material{
    String descripcion;

    public Otros(String descripcion) {
        this.descripcion = descripcion;
    }

    public Otros() {
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Otros{" +
                "descripcion='" + descripcion + '\'' +
                '}';
    }
}
