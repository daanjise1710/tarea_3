package jimenez.darren.bl.entities;

import java.util.Date;

public class Profesor extends Usuario {
    String cedula;
    String tipoContrato;
    Date fechaContratacion;

    public Profesor(String nombre, String apellido, String cedula, String tipoContrato, Date fechaContratacion) {
        super(nombre, apellido);
        this.cedula = cedula;
        this.tipoContrato = tipoContrato;
        this.fechaContratacion = fechaContratacion;
    }

    public Profesor(String cedula, String tipoContrato, Date fechaContratacion) {
        this.cedula = cedula;
        this.tipoContrato = tipoContrato;
        this.fechaContratacion = fechaContratacion;
    }

    public Profesor() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Date fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "cedula='" + cedula + '\'' +
                ", tipoContrato='" + tipoContrato + '\'' +
                ", fechaContratacion=" + fechaContratacion +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
