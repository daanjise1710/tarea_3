package jimenez.darren.bl.entities;

import jimenez.darren.bl.entities.Material;

import java.util.Arrays;
import java.util.Date;

public class Audio_Visual extends Material {
    String formato;
    String duracion;
    String idioma;

    public Audio_Visual(String formato, String duracion, String idioma) {
        this.formato = formato;
        this.duracion = duracion;
        this.idioma = idioma;
    }

    public Audio_Visual(String signatura, Date fechaCompra, String restringido, String tema, String formato, String duracion, String idioma) {
        super(signatura, fechaCompra, restringido, tema);
        this.formato = formato;
        this.duracion = duracion;
        this.idioma = idioma;
    }

    public Audio_Visual() {
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "Audio_Visual{" +
                "formato='" + formato + '\'' +
                ", duracion='" + duracion + '\'' +
                ", idioma='" + idioma + '\'' +
                ", signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
