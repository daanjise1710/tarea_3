package jimenez.darren.ui;

import javax.sound.midi.Track;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.*;

public class Main {


    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void tipoUsuario(String [] datosUsuario) throws IOException {
        out.println("Digite el nombre del usuario");
        datosUsuario[0] = in.readLine();

        out.println("Digite el apellido del usuario");
        datosUsuario[1] = in.readLine();
    }

    public static void tipoAdministrativo() throws IOException {
        String nombre,apellido,cedula, tipoNombramiento;
        String[] datosUsuario = new String[2];
        int horasSemanales, opc = -1;

        tipoUsuario(datosUsuario);
        
        out.println("Digite la cedula del usuario");
        cedula = in.readLine();

        out.println("Digite el tipo de nombramiento que tiene el usuario (A, B o C");

        do {
            tipoNombramiento = in.readLine();

            switch (tipoNombramiento){
                case "A":
                case "a":
                    tipoNombramiento="A";
                    opc = 1;

                    break;

                case "B":
                case "b":
                    tipoNombramiento="B";
                    opc = 1;
                    break;

                case "C":
                case "c":
                    tipoNombramiento="C";
                    opc = 1;
                    break;

                default:
                    out.println("Tipo de nombramiento invalido, solo se permiten las opciones A, B o C");
                    out.print("Ingrese nuevamente el tipo de nombramiento acontinuacion - ");
                    opc = 2;
                    break;
            }
        }while(opc == 2);

        out.println("Digite la cantidad de horas semanales que trabaja el usuario");
        horasSemanales = Integer.parseInt(in.readLine());

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String query = "INSERT INTO  administrativo(NOMBRE,APELLIDO,CEDULA,TIPO_NOMBRAMIENTO,HORAS_SEMANALES) VALUES('"+datosUsuario[0]+"','"+datosUsuario[1]+"','"+cedula+"','"+tipoNombramiento+"',"+horasSemanales+");";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void tipoProfesor() throws IOException {
        String nombre,apellido,cedula, tipoContrato = null, fechaContratacion;
        String[] datosUsuario = new String[2];
        int opc = -1;

        tipoUsuario(datosUsuario);

        out.println("Digite la cedula del usuario");
        cedula = in.readLine();

        out.println("Digite el tipo de contrato que tiene el usuario");

        do {
            out.println("1.Tiempo completo");
            out.println("2.Medio tiempo");
            opc = Integer.parseInt(in.readLine());

            switch (opc){
                case 1:
                    tipoContrato = "Tiempo completo";
                    break;
                    
                case 2:
                    tipoContrato = "Medio tiempo";
                    break;
                    
                default:
                    out.println("Tipo de contrato invalido");
                    out.print("Ingrese el numero correspondiente al tipo de contrato");
                    break;
            }
        }while(opc != 1 && opc != 2);

        System.out.println("Ingrese la fecha de contratacion del usuario con el siguiente formato (yyyy-MM-dd)");
        fechaContratacion = in.readLine();

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String query = "INSERT INTO  profesor(NOMBRE,APELLIDO,CEDULA,TIPO_CONTRATO,FECHA_CONTRATACION) VALUES('"+datosUsuario[0]+"','"+datosUsuario[1]+"','"+cedula+"','"+tipoContrato+"','"+fechaContratacion+"')";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void tipoEstudiante() throws IOException {
        String nombre,apellido,numCarne,carrera;
        String[] datosUsuario = new String[2];
        int creditos;

        tipoUsuario(datosUsuario);

        out.println("Digite el numero de carne del estudiante");
        numCarne = in.readLine();

        out.println("Digite el nombre de la carrera que esta llevando el estudiante");
        carrera = in.readLine();

        out.println("Digite el numero de creditos matriculados en el cuatrimestre actual por el estudiante");
        creditos = Integer.parseInt(in.readLine());

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String query = "INSERT INTO  estudiante(NOMBRE,APELLIDO,NUMERO_CARNE,CARRERA,CREDITOS) VALUES('"+datosUsuario[0]+"','"+datosUsuario[1]+"','"+numCarne+"','"+carrera+"',"+creditos+")";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void guardarUsuario() throws IOException {
        int opc = -1;

            do {
                out.println("---REGISTRO DE USUARIOS---");
                out.println("1.Administrativo");
                out.println("2.Profesor");
                out.println("3.Estudiante");
                out.println("4.Cancelar");
                out.println("Ingrese una opción");
                opc = Integer.parseInt(in.readLine());

                switch (opc){
                    case 1:
                        tipoAdministrativo();
                        break;
                    case 2:
                        tipoProfesor();
                        break;
                    case 3:
                        tipoEstudiante();
                        break;
                    case 4:
                        out.println("Regresando al menu principal");
                        break;
                    default:
                        out.println("Tipo de usuario invalido, ingrese uno de los usuarios en la lista");
                        break;
                }
            }while(opc != 4);
    }

    public static void tipoMaterial(String[] datosMaterial) throws IOException {
        String opc;
        int control = -1;

        out.println("Ingrese la signatura del material");
        datosMaterial[0] = in.readLine();

        out.println("Ingrese la fecha de compra del material usando el siguiente formato (yyyy-MM-dd");
        datosMaterial[1] = in.readLine();

        do {
            out.println("Es restringido? S/N");
            opc = in.readLine();

            switch (opc){
                case "S":
                case "s":
                    datosMaterial[2]="Restringido";
                    control = 1;
                    break;

                case "N":
                case "n":
                    datosMaterial[2]="No Restringido";
                    control = 1;
                    break;

                default:
                    out.println("Respuesta Invalida, solo puede contestar S/N");
                    control = -1;
                    break;
            }
        }while(control == -1);

        out.println("Ingrese el tema del material");
        datosMaterial[3] = in.readLine();
    }

    public static void tipoTexto() throws IOException {
        String titulo,nombreAutor,fechaPublicacion,idioma;
        String[] datosMaterial = new String[4];
        int numPaginas;

        tipoMaterial(datosMaterial);

        out.println("Ingrese el titulo del texto");
        titulo = in.readLine();

        out.println("Ingrese el nombre del autor del texto");
        nombreAutor = in.readLine();

        out.println("Ingrese la fecha de publicacion del texto usando el siguiente formato (dd/MM/yyyy)");
        fechaPublicacion = in.readLine();

        out.println("Ingrese el numero de paginas que tiene el texto");
        numPaginas = Integer.parseInt(in.readLine());

        out.println("Ingrese el idioma del texto");
        idioma = in.readLine();

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                String query = "INSERT INTO  texto(SIGNATURA,FECHA_COMPRA,RESTRINGIDO,TEMA,TITULO,AUTOR,FECHA_PUBLICACION,PAGINAS,IDIOMA) VALUES('"+datosMaterial[0]+"','"+datosMaterial[1]+"','"+datosMaterial[2]+"','"+datosMaterial[3]+"','"+titulo+"','"+nombreAutor+"','"+fechaPublicacion+"',"+numPaginas+",'"+idioma+"')";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void tipoAudio() throws IOException {
        String formato = null,duracion,idioma;
        String[] datosMaterial = new String[4];
        int opc=-1;

        tipoMaterial(datosMaterial);

        out.println("Ingrese el formato del audio");

        do {
            out.println("1.Casete");
            out.println("2.CD");
            out.println("3.DVD");

            opc = Integer.parseInt(in.readLine());

            switch (opc) {
                case 1:
                    formato = "Casete";
                    break;
                case 2:
                    formato = "CD";
                    break;
                case 3:
                    formato = "DVD";
                    break;
                default:
                    out.println("Opcion invalida, ingrese el numero correspondiente al formato de la lista");
            }
        }while (opc != 1 && opc != 2 && opc != 3);

        out.println("Ingrese la duracion del audio usando el formato (HH:mm:ss)");
        duracion = in.readLine();

        out.println("Ingrese el idioma en el que se encuentra el audio");
        idioma = in.readLine();

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String query = "INSERT INTO  audio(SIGNATURA,FECHA_COMPRA,RESTRINGIDO,TEMA,FORMATO,DURACION,IDIOMA) VALUES('"+datosMaterial[0]+"','"+datosMaterial[1]+"','"+datosMaterial[2]+"','"+datosMaterial[3]+"','"+formato+"','"+duracion+"','"+idioma+"')";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void tipoVideo() throws IOException {
        String formato = null,duracion,idioma,director;
        String[] datosMaterial = new String[4];
        int opc=-1;

        tipoMaterial(datosMaterial);

        out.println("Ingrese el formato del video");

        do {
            out.println("1.VHS");
            out.println("2.VCD");
            out.println("3.DVD");

            opc = Integer.parseInt(in.readLine());

            switch (opc) {
                case 1:
                    formato = "VHS";
                    break;
                case 2:
                    formato = "VCD";
                    break;
                case 3:
                    formato = "DVD";
                    break;
                default:
                    out.println("Opcion invalida, ingrese el numero correspondiente al formato de la lista");
            }
        }while (opc != 1 && opc != 2 && opc != 3);

        out.println("Ingrese la duracion del video usando el formato (HH:mm:ss)");
        duracion = in.readLine();

        out.println("Ingrese el idioma en el que se encuentra el video");
        idioma = in.readLine();

        out.println("Ingrese el nombre del director del video");
        director = in.readLine();

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String query = "INSERT INTO  video(SIGNATURA,FECHA_COMPRA,RESTRINGIDO,TEMA,FORMATO,DURACION,IDIOMA,DIRECTOR) VALUES('"+datosMaterial[0]+"','"+datosMaterial[1]+"','"+datosMaterial[2]+"','"+datosMaterial[3]+"','"+formato+"','"+duracion+"','"+idioma+"','"+director+"')";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void tipoOtros() throws IOException {
        String  descripcion;
        String[] datosMaterial = new String[4];

        tipoMaterial(datosMaterial);

        out.println("Ingrese la descripcion del producto");
        descripcion = in.readLine();

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String query = "INSERT INTO  otro(SIGNATURA,FECHA_COMPRA,RESTRINGIDO,TEMA,DESCRIPCION) VALUES('"+datosMaterial[0]+"','"+datosMaterial[1]+"','"+datosMaterial[2]+"','"+datosMaterial[3]+"','"+descripcion+"')";
            Connection conn = null;
            Statement stmt = null;
            String connectionUrl = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn = DriverManager.getConnection(connectionUrl);
            stmt = conn.createStatement();
            stmt.executeQuery(query);
            System.out.println("Registro exitoso");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void guardarMaterial() throws IOException {
        int opc = -1;

        out.println("Digite el numero correspondiente al material que desea registrar");

        do {
            out.println("1.Texto");
            out.println("2.Audio");
            out.println("3.Video");
            out.println("4.Otro");
            out.println("5.Cancelar");

            opc = Integer.parseInt(in.readLine());

            switch (opc){
                case 1:
                    tipoTexto();
                    break;
                case 2:
                    tipoAudio();
                    break;
                case 3:
                    tipoVideo();
                    break;
                case 4:
                    tipoOtros();
                    break;
                case 5:
                    out.println("Regresando al menu prncipal");
                    break;
                default:
                    out.println("Tipo de material invalido, ingrese uno de los materiales en la lista");
                    break;
            }
        }while(opc != 5);
    }

    public static void imprimirAdministrativos(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM administrativo";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO USUARIOS ADMINISTRATIVOS---");
            out.println("---NOMBRE, APELLIDO, CEDULA, TIPO DE NOMBRAMIENTO, HORAS SEMANALES---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.println(rs.getString(5));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirProfesores(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM profesor";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO USUARIOS PROFESOR---");
            out.println("---NOMBRE, APELLIDO, CEDULA, TIPO DE CONTRATO, FECHA CONTRATACION---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.println(rs.getString(5));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirEstudiantes(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM estudiante";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO USUARIOS ESTUDIANTES---");
            out.println("---NOMBRE, APELLIDO, NUMERO DE CARNE, CARRERA, CREDITOS---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.println(rs.getString(5));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirTodosUsuarios(){
        out.println("---IMPRIMIENDO TODOS LOS USUARIOS---");
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirAdministrativos();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirProfesores();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirEstudiantes();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
    }

    public static void imprimirUsuarios() throws IOException {
        int opc = -1;
        do {
            out.println("1.Imprimir usuarios tipo administrativo");
            out.println("2.Imprimir usuarios tipo profesor");
            out.println("3.Imprimir usuarios tipo estudiante");
            out.println("4.Imprimir todos los usuarios");
            out.println("5.Salir");
            out.println("Digite una opción");
            opc = Integer.parseInt(in.readLine());

            switch (opc) {
                case 1:
                    imprimirAdministrativos();
                    break;

                case 2:
                    imprimirProfesores();
                    break;

                case 3:
                    imprimirEstudiantes();
                    break;

                case 4:
                    imprimirTodosUsuarios();
                    break;

                case 5:
                    out.println("Volviendo al menu principal");
                    break;

                default:
                    out.println("Opcin no valida, seleccione uno de los numeros de la lista");
                    break;
            }
        }while(opc != 5);
    }

    public static void imprimirTexto() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM texto";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO MATERIALES TIPO TEXTO---");
            out.println("---SIGNATURA, FECHA DE COMPRA, RESTRINGIDO, TEMA, TITULO, NOMBRE DE AUTOR, FECHA DE PUBLICACION, NUMERO DE PAGINAS, IDIOMA---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.print(rs.getString(5)+", ");
                System.out.print(rs.getString(6)+", ");
                System.out.print(rs.getString(7)+", ");
                System.out.print(rs.getString(8)+", ");
                System.out.println(rs.getString(9));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirAudio() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM audio";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO MATERIALES TIPO AUDIO---");
            out.println("---SIGNATURA, FECHA DE COMPRA, RESTRINGIDO, TEMA, FORMATO, DURACION, IDIOMA---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.print(rs.getString(5)+", ");
                System.out.print(rs.getString(6)+", ");
                System.out.println(rs.getString(7));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirVideo() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM video";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO MATERIALES TIPO VIDEO---");
            out.println("---SIGNATURA, FECHA DE COMPRA, RESTRINGIDO, TEMA, FORMATO, DURACION, IDIOMA, DIRECTOR---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.print(rs.getString(5)+", ");
                System.out.print(rs.getString(6)+", ");
                System.out.print(rs.getString(7)+", ");
                System.out.println(rs.getString(8));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirOtros() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM otro";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=tarea_cuatro_POO;user=sa;password=daanjise1710";
            conn =  DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO MATERIALES TIPO OTROS---");
            out.println("---SIGNATURA, FECHA DE COMPRA, RESTRINGIDO, TEMA, DESCRIPCION---");
            while(rs.next()){
                System.out.print(rs.getString(1)+" ");
                System.out.print(rs.getString(2)+", ");
                System.out.print(rs.getString(3)+", ");
                System.out.print(rs.getString(4)+", ");
                System.out.println(rs.getString(5));
            }
        }
        catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public static void imprimirTodosMateriales() {
        out.println("---IMPRIMIENDO TODOS LOS MATERIALES---");
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirTexto();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirAudio();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirVideo();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
        imprimirOtros();
        out.println("- - - - - - - - - - - - - - - - - - - - - - - -");
    }

    public static void imprimirMateriales() throws IOException {
        int opc = -1;
        do {
            out.println("1.Imprimir materiales tipo texto");
            out.println("2.Imprimir materiales tipo audio");
            out.println("3.Imprimir materiales tipo video");
            out.println("4.Imprimir materiales tipo otros");
            out.println("5.Imprimir todos los materiales");
            out.println("6.Salir");
            out.println("Digite una opción");
            opc = Integer.parseInt(in.readLine());

            switch (opc) {
                case 1:
                    imprimirTexto();
                    break;

                case 2:
                    imprimirAudio();
                    break;

                case 3:
                    imprimirVideo();
                    break;

                case 4:
                    imprimirOtros();
                    break;

                case 5:
                    imprimirTodosMateriales();
                    break;

                case 6:
                    out.println("Volviendo al menu principal");
                    break;

                default:
                    out.println("Opcin no valida, seleccione uno de los numeros de la lista");
                    break;
            }
        }while(opc != 6);
    }

    public static void menu() throws IOException{
        int opcion = -1;
        do {
            out.println("1.Registrar usuario");
            out.println("2.Registrar material");
            out.println("3.Imprimir usuarios");
            out.println("4.Imprimir materiales");
            out.println("5.Salir");
            out.println("Digite una opción");
            opcion = Integer.parseInt(in.readLine());

            switch (opcion) {
                case 1:
                    guardarUsuario();
                    break;

                case 2:
                    guardarMaterial();
                    break;

                case 3:
                    imprimirUsuarios();
                    break;

                case 4:
                    imprimirMateriales();
                    break;

                case 5:
                    out.println("Cerrando programa");
                    break;

                default:
                    out.println("Opcin no valida, seleccione uno de los numeros de la lista");
                    break;
            }
        }while(opcion!= 5);
    }

    public static void main(String[] args) throws IOException, ParseException {
        menu();
    }
}
