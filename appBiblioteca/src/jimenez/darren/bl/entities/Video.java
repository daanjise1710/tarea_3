package jimenez.darren.bl.entities;

import java.util.Arrays;
import java.util.Date;

public class Video extends Audio_Visual {
    String director;

    public Video(String formato, String duracion, String idioma, String director) {
        super(formato, duracion, idioma);
        this.director = director;
    }

    public Video(String signatura, Date fechaCompra, String restringido, String tema, String formato, String duracion, String idioma, String director) {
        super(signatura, fechaCompra, restringido, tema, formato, duracion, idioma);
        this.director = director;
    }

    public Video(String director) {
        this.director = director;
    }

    public Video() {
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "Video{" +
                "director='" + director + '\'' +
                ", formato='" + formato + '\'' +
                ", duracion='" + duracion + '\'' +
                ", idioma='" + idioma + '\'' +
                ", signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
