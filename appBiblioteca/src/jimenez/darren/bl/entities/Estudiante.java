package jimenez.darren.bl.entities;

import jimenez.darren.bl.entities.Usuario;

public class Estudiante extends Usuario {
    String numCarne;
    String carrera;
    int creditos;

    public Estudiante(String nombre, String apellido, String numCarne, String carrera, int creditos) {
        super(nombre, apellido);
        this.numCarne = numCarne;
        this.carrera = carrera;
        this.creditos = creditos;
    }

    public Estudiante(String numCarne, String carrera, int creditos) {
        this.numCarne = numCarne;
        this.carrera = carrera;
        this.creditos = creditos;
    }

    public Estudiante() {
    }

    public String getNumCarne() {
        return numCarne;
    }

    public void setNumCarne(String numCarne) {
        this.numCarne = numCarne;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    @Override
    public String toString() {
        return "Estudiante{" +
                "numCarne='" + numCarne + '\'' +
                ", carrera='" + carrera + '\'' +
                ", creditos=" + creditos +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
