package jimenez.darren.bl.entities;

import jimenez.darren.bl.entities.Usuario;

public class Administrativo extends Usuario {
    String cedula;
    String tipoNombramiento;
    String horasSemanales;

    public Administrativo(String nombre, String apellido, String cedula, String tipoNombramiento, String horasSemanales) {
        super(nombre, apellido);
        this.cedula = cedula;
        this.tipoNombramiento = tipoNombramiento;
        this.horasSemanales = horasSemanales;
    }

    public Administrativo(String cedula, String tipoNombramiento, String horasSemanales) {
        this.cedula = cedula;
        this.tipoNombramiento = tipoNombramiento;
        this.horasSemanales = horasSemanales;
    }

    public Administrativo() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTipoNombramiento() {
        return tipoNombramiento;
    }

    public void setTipoNombramiento(String tipoNombramiento) {
        this.tipoNombramiento = tipoNombramiento;
    }

    public String getHorasSemanales() {
        return horasSemanales;
    }

    public void setHorasSemanales(String horasSemanales) {
        this.horasSemanales = horasSemanales;
    }

    @Override
    public String toString() {
        return "Administrativo{" +
                "cedula='" + cedula + '\'' +
                ", tipoNombramiento='" + tipoNombramiento + '\'' +
                ", horasSemanales='" + horasSemanales + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
